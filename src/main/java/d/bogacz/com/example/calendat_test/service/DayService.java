package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.repository.DayRepository;
import d.bogacz.com.example.calendat_test.service.exception.DayNotFoundException;
import d.bogacz.com.example.calendat_test.utilities.TimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service
public class DayService {


    @Autowired
    DayRepository dayRepository;


    public Day createDay() {
        Day day = new Day();
        day.setId(String.valueOf(UUID.randomUUID()));
        day.setDate(LocalDate.now());
        return day;
    }

    public List<Day> createDays(int x) {
        List<Day> dayList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        for (int i = 0; i < x; i++) {
            localDate=localDate.plusDays(1);
            Day day = new Day();
            day.setId(String.valueOf(UUID.randomUUID()));
            day.setDate(localDate);
            dayList.add(day);
        }
        return dayList;
    }

    public List<Day> createRestoOfCurentMonthDays() {
        int curentMonth = TimeUtilities.getCutentMonth();

        List<Day> daysInCurentMonthList = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        while (true) {
            localDate=localDate.plusDays(1);
            int monthFromaData = localDate.getMonthValue();
            if (curentMonth == monthFromaData) {
                Day day = new Day();
                day.setId(String.valueOf(UUID.randomUUID()));
                day.setDate(localDate);
                daysInCurentMonthList.add(day);
            } else {
                break;
            }
        }
        return daysInCurentMonthList;
    }

    public List<Day> createDaysInChosenMonth(int month, int year) {
        List<Day> daysInChosenMonth = new ArrayList<>();
        LocalDate localDate = LocalDate.now();
        localDate=localDate.of(year, month, 1);

        while (localDate.getMonthValue() == month) {
            Day day = new Day();
            day.setId(String.valueOf(UUID.randomUUID()));
            day.setDate(localDate);
            daysInChosenMonth.add(day);
            localDate=localDate.plusDays(1);
        }
        return daysInChosenMonth;
    }

    public List<Day> getAllDays() {
        return  dayRepository.findAll();
    }

    public Day findDayById(String id) throws DayNotFoundException {
        Optional<Day> d = dayRepository.findById(id);
        return d.orElseThrow(DayNotFoundException::new);
    }

    public void setStartAndFinishVisitsTime(Day day) {
        LocalDate localDate = day.getDate();
        String dayOfWeek = String.valueOf(localDate.getDayOfWeek());
        if (!dayOfWeek.equals("SATURDAY") && !dayOfWeek.equals("SUNDAY")) {
            day.setStartTime(BigDecimal.valueOf(15.00));
            day.setFinishTime(BigDecimal.valueOf(21.00));
        }
    }
    public void deleteById(String id) {
        dayRepository.deleteById(id);
    }
}