package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Role;
import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.repository.RoleRepository;
import d.bogacz.com.example.calendat_test.repository.UserRepository;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleReposiotry;

    public void saveUser(User user) {
        user.setUserId(String.valueOf(UUID.randomUUID()));
        Role role = roleReposiotry.findByRole("Role_User");
        System.out.println(role);
        user.setRoles(new HashSet<>(Arrays.asList(role)));
        userRepository.save(user);
    }

    public User findaUserById(String id) throws UserNotFoundException {
        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException());
        return user;
    }
}
