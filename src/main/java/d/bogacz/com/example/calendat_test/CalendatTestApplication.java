package d.bogacz.com.example.calendat_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalendatTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalendatTestApplication.class, args);
	}

}
