package d.bogacz.com.example.calendat_test;

import d.bogacz.com.example.calendat_test.domain.DomainFactory;
import d.bogacz.com.example.calendat_test.domain.ports.offered.CalendatService;
import d.bogacz.com.example.calendat_test.domain.ports.required.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CalendatConfiguration {

    @Bean
    CalendatService calendatService(@Autowired UserRepository userRepository) {
        return DomainFactory.createCalendatService(userRepository);
    }

}
