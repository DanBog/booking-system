package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.repository.VisitRepository;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import d.bogacz.com.example.calendat_test.service.exception.VisitNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class VisitService {


    @Autowired
    VisitRepository visitRepository;

    @Autowired
    UserService userService;

    public List<Visit> getVisitsFromSelectedDay(String dayId) {
        return visitRepository.findVisitForSelectedDay(dayId);

    }

    public Visit createVisit(BigDecimal startTime, BigDecimal finishTime) {

        Visit visit = new Visit(startTime, finishTime);
        visit.setVisitId(String.valueOf(UUID.randomUUID()));
        return visit;
    }

    public Visit findById(String id) throws VisitNotFoundException {

        Optional<Visit> v = visitRepository.findById(id);
        return v.orElseThrow(VisitNotFoundException::new);
    }

    public List<Visit> getAllUnreservedVisits() {
        return visitRepository.findAllUnreservedVisit();
    }

    @Transactional
    public void bookVisit(String userId, String visitId) throws UserNotFoundException, VisitNotFoundException {
        User user = userService.findaUserById(userId);
        Visit visit = findById(visitId);
        visitRepository.bookVisit(userId, visitId);
    }
}
