package d.bogacz.com.example.calendat_test.adapters;

import d.bogacz.com.example.calendat_test.domain.ports.required.UserRepository;
import d.bogacz.com.example.calendat_test.model.User;
import org.springframework.stereotype.Component;

@Component
public class JpaUserRepositoryAdapter implements UserRepository {

    private d.bogacz.com.example.calendat_test.repository.UserRepository jpaUserRepository;

    @Override
    public void save(User user) {
        jpaUserRepository.save(user);
    }

    @Override
    public User findById(String id) {
        return jpaUserRepository.getOne(id);
    }
}
