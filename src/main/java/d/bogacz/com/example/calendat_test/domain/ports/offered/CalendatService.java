package d.bogacz.com.example.calendat_test.domain.ports.offered;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

public interface CalendatService {

    void registerVisit(String id, Visit visit);

    @AllArgsConstructor
    @Data
    class Visit {
        ZonedDateTime start;
        ZonedDateTime end;
    }
}
