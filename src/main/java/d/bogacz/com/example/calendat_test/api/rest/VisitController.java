package d.bogacz.com.example.calendat_test.api.rest;

import d.bogacz.com.example.calendat_test.api.RegisterVisitRequestDto;
import d.bogacz.com.example.calendat_test.domain.ports.offered.CalendatService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class VisitController {

    private CalendatService calendatService;

    public void registerVisit(RegisterVisitRequestDto registerVisitRequestDto) {
        calendatService.registerVisit(registerVisitRequestDto.getUserId(),
                new CalendatService.Visit(
                        registerVisitRequestDto.getStart(),
                        registerVisitRequestDto.getStart()
                ));
    }
}
