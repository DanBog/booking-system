package d.bogacz.com.example.calendat_test.utilities;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Service
public class TimeUtilities {

    private static Date date = new Date();

    public static int getCutentMonth() {
        LocalDate localDate = LocalDate.now();
        return localDate.getMonthValue();
    }

    public static int getCutentDayOfMonth() {
        LocalDate localDate = LocalDate.now();
        return localDate.getDayOfMonth();
    }

    public static int getCutentYear() {
        LocalDate localDate = LocalDate.now();
        return localDate.getYear();
    }

    public static LocalDate getCurentDate() {
        return LocalDate.now();
    }
}
