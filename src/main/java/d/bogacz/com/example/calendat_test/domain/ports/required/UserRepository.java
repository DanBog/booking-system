package d.bogacz.com.example.calendat_test.domain.ports.required;

import d.bogacz.com.example.calendat_test.model.User;

public interface UserRepository {

    void save(User user);

    User findById(String id);
}
