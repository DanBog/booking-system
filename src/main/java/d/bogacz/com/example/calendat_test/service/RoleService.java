package d.bogacz.com.example.calendat_test.service;


import d.bogacz.com.example.calendat_test.model.Role;
import org.springframework.stereotype.Service;

@Service
public class RoleService {


    public Role createRole(String rola, int id) {
        Role role = new Role();
        role.setRole(rola);
        role.setRoleId(id);
        return role;
    }
}
