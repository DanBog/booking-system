package d.bogacz.com.example.calendat_test.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Day {


    @Id
    private String id;
    private LocalDate date;
    private BigDecimal startTime;
    private BigDecimal finishTime;
    @OneToMany
    @JoinColumn(name = "day_id")
    private List<Visit> visitList;


}
