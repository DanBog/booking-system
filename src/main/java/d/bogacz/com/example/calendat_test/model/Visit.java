package d.bogacz.com.example.calendat_test.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Visit {


    @Id
    private String visitId;
    private BigDecimal statTime;
    private BigDecimal finishTime;
    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    public Visit(BigDecimal statTime, BigDecimal finishTime) {
        this.statTime = statTime;
        this.finishTime = finishTime;
    }

}
