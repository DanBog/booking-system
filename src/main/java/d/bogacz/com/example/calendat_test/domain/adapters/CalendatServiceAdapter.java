package d.bogacz.com.example.calendat_test.domain.adapters;

import d.bogacz.com.example.calendat_test.domain.ports.offered.CalendatService;
import d.bogacz.com.example.calendat_test.domain.ports.required.UserRepository;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class CalendatServiceAdapter implements CalendatService {

    private UserRepository userRepository;

    @Override
    public void registerVisit(String id, Visit visit) {
        Optional.ofNullable(userRepository.findById(id))
                .ifPresent(user -> {
//                    user.setVisits(new ArrayList<>(new Visit()));
                });
    }
}
