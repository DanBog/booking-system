package d.bogacz.com.example.calendat_test.domain;

import d.bogacz.com.example.calendat_test.domain.adapters.CalendatServiceAdapter;
import d.bogacz.com.example.calendat_test.domain.ports.offered.CalendatService;
import d.bogacz.com.example.calendat_test.domain.ports.required.UserRepository;

public class DomainFactory {

    public static CalendatService createCalendatService(UserRepository userRepository) {
        return new CalendatServiceAdapter(userRepository);
    }

}
