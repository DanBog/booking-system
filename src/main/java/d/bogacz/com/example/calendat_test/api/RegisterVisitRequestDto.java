package d.bogacz.com.example.calendat_test.api;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class RegisterVisitRequestDto {

    private String userId;
    private ZonedDateTime start;
    private ZonedDateTime end;

}
