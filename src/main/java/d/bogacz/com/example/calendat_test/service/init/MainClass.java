package d.bogacz.com.example.calendat_test.service.init;

import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.model.Role;
import d.bogacz.com.example.calendat_test.model.User;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.repository.RoleRepository;
import d.bogacz.com.example.calendat_test.repository.UserRepository;
import d.bogacz.com.example.calendat_test.service.*;
import d.bogacz.com.example.calendat_test.service.exception.UserNotFoundException;
import d.bogacz.com.example.calendat_test.service.exception.VisitNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class MainClass {

    @Autowired
    DayService dayService;
    @Autowired
    DayVisitService dayVisitService;
    @Autowired
    VisitService visitService;
    @Autowired
    RoleService roleService;
    @Autowired
    RoleRepository roleReposiotry;
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    @PostConstruct
    public void init() throws VisitNotFoundException, UserNotFoundException {
        System.out.println("klucz UUID");
        System.out.println(UUID.randomUUID());
        Role roleAdmin = roleService.createRole("Role_Admin", 1);
        Role roleUser = roleService.createRole("Role_User", 2);

        roleReposiotry.save(roleAdmin);
        roleReposiotry.save(roleUser);

        User user = new User();
        user.setName("Adam");
        user.setLastName("Kowalski");
        user.setEmail("adamkowalski@onet.eu");
        userService.saveUser(user);
        String userId = user.getUserId();
        System.out.println("user Id"+user.getUserId());

        List<Day> dayList = dayService.createDaysInChosenMonth(8, 2019);
        dayVisitService.setVisitsToDays(dayList);

        List<Visit> unreservedVisits = visitService.getAllUnreservedVisits();

        List<Visit> forUser = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            String id = unreservedVisits.get(i).getVisitId();
            System.out.println("id wizyty " + id);
            visitService.bookVisit(userId, id);
           // forUser.add(unreservedVisits.get(i));
        }
        user.setVisits(forUser);
        userRepository.save(user);
        System.out.println(user);

    }
}

