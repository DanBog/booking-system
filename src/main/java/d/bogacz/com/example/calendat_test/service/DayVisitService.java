package d.bogacz.com.example.calendat_test.service;

import d.bogacz.com.example.calendat_test.model.Day;
import d.bogacz.com.example.calendat_test.model.Visit;
import d.bogacz.com.example.calendat_test.repository.DayRepository;
import d.bogacz.com.example.calendat_test.repository.VisitRepository;
import d.bogacz.com.example.calendat_test.utilities.TimeUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class DayVisitService {

    @Autowired
    DayService dayService;
    @Autowired
    DayRepository dayRepository;
    @Autowired
    VisitRepository visitRepository;
    @Autowired
    VisitService visitService;



    public void setVisitsToDays(List<Day> dayList) {

        for (Day day : dayList
        ) {
            dayService.setStartAndFinishVisitsTime(day);
            if (day.getStartTime() != null) {
                BigDecimal start = day.getStartTime();
                BigDecimal finish = day.getFinishTime();
                BigDecimal finishTimeCurrentVisit = start;
                day.setVisitList(new ArrayList<>());
                do {
                    finishTimeCurrentVisit = finishTimeCurrentVisit.add(BigDecimal.valueOf(0.50));
                    Visit visit = visitService.createVisit(start, finishTimeCurrentVisit);
                    day.getVisitList().add(visit);
                    visitRepository.save(visit);
                    start = finishTimeCurrentVisit;

                } while (!finish.equals(finishTimeCurrentVisit));
                dayRepository.save(day);
            }
        }
    }

}

