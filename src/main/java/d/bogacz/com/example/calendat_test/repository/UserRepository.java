package d.bogacz.com.example.calendat_test.repository;

import d.bogacz.com.example.calendat_test.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
