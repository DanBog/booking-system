package d.bogacz.com.example.calendat_test.domain;

import d.bogacz.com.example.calendat_test.domain.ports.required.UserRepository;
import d.bogacz.com.example.calendat_test.model.User;

import java.util.HashSet;
import java.util.Set;

public class InMemoryUserRepository implements UserRepository {

    private Set<User> users = new HashSet<>();

    @Override
    public void save(User user) {
        if (users.contains(user)) {
            users.remove(user);
        }
    }

    @Override
    public User findById(String id) {

        return null;
    }
}

