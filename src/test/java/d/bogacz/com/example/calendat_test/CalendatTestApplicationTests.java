package d.bogacz.com.example.calendat_test;

import d.bogacz.com.example.calendat_test.domain.ports.offered.CalendatService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalendatTestApplicationTests {

	@Autowired
	private CalendatService calendatService;

	@Test
	public void contextLoads() {
	}

}
